var result = "0";
var computation = "";

$(document).ready(function() {
    $(".button").click(function() {
        switch (this.id) {
            case "AC":
                computation = "";
                $(".result > span").text("0");
                break;
            case "equal":
                updateResult();
                computation = result;
                break;
            case "pm":
                computation = "-" + computation;
                updateResult();
                break;
            case "pc":
                computation += "/100";
                updateResult();
                break;
            case "plus":
                computation += "+";
                updatePresent();
                break;
            case "minus":
                computation += "-";
                updatePresent();
                break;
            case "divide":
                computation += "/";
                updatePresent();
                break;
            case "multi":
                computation += "*";
                updatePresent();
                break;
            case "dot":
                computation += ".";
                updatePresent();
                break;
            default:
                computation += this.id.substring(1);
                updatePresent();
        }
    })
});

var updateResult = function() {
    result = eval(computation);
    strResult = result.toString();
    if (strResult.indexOf(".") > -1 && 
        strResult.length - strResult.indexOf(".") > 5) {
        result = result.toFixed(3);
    }
    $(".result > span").text(result);
    computation = result;
};

var updatePresent = function() {
    $(".result > span").text(computation);
}